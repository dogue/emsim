#![allow(dead_code)]
use bevy::prelude::*;

use crate::ui::UiTile;

pub const TILE_SCALE: f32 = 4.0;
pub const TILE_SIZE: f32 = 12.0;

pub struct TilePlugin;

impl Plugin for TilePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(TileSheet::default())
            .add_startup_system_to_stage(StartupStage::PreStartup, load_tiles)
            .add_system(update_sprite);
    }
}

#[derive(Clone)]
pub enum TileSprite {
    WallWood = 0,
    WallWood2 = 1,
    WallWood3 = 2,
    WallWood4 = 3,
    WallWood5 = 4,
    WallWood6 = 5,
    WallWood7 = 6,
    WallWood8 = 7,
    WallWood9 = 8,
    WallWood10 = 9,
    StairsDown = 10,
    StairsUp = 11,
    Ladder = 12,
    Hatch = 13,
    Hatch2 = 14,
    Hatch3 = 15,
    Empty = 16,
    DoorWay = 20,
    DoorWay2 = 21,
    LargeDoorwayLeft = 22,
    LargeDoorwayRight = 23,
    Door = 24,
    Door2 = 25,
    LargeDoorLeft = 26,
    LargeDoorRight = 27,
    Dirt = 40,
    Dirt2 = 41,
    Dirt3 = 42,
    DarkGrass = 52,
    DarkGrass2 = 53,
    Tree = 80,
    Tree2 = 81,
    Tree3 = 82,
    Tree4 = 83,
    Tree5 = 84,
    Man = 161,
    Campfire = 206,
}

impl From<TileSprite> for usize {
    fn from(variant: TileSprite) -> usize {
        variant as usize
    }
}

#[derive(Default, Clone)]
pub struct TileSheet {
    handle: Handle<TextureAtlas>,
}

impl TileSheet {
    pub fn set_handle(&mut self, handle: Handle<TextureAtlas>) {
        self.handle = handle;
    }

    pub fn handle(&self) -> Handle<TextureAtlas> {
        self.handle.clone()
    }
}

#[derive(Component, Clone)]
pub struct Tile {
    sprite: TileSprite,
}

impl Tile {
    pub fn new() -> Self {
        Self {
            sprite: TileSprite::Empty,
        }
    }

    pub fn with_sprite(mut self, sprite: TileSprite) -> Self {
        self.sprite = sprite;
        self
    }

    pub fn sprite(&self) -> TileSprite {
        self.sprite.clone()
    }

    pub fn set_sprite(&mut self, sprite: TileSprite) {
        self.sprite = sprite;
    }
}

fn load_tiles(
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut tile_sheet: ResMut<TileSheet>,
) {
    let texture_handle = asset_server.load("tilesheet.png");
    let texture_atlas = TextureAtlas::from_grid_with_padding(
        texture_handle,
        Vec2::new(12.0, 12.0),
        20,
        20,
        Vec2::new(1.0, 1.0),
        Vec2::new(1.0, 1.0),
    );
    tile_sheet.set_handle(texture_atlases.add(texture_atlas));
}

fn update_sprite(
    mut query: Query<(&mut TextureAtlasSprite, &Tile), Or<(Changed<Tile>, Changed<UiTile>)>>,
) {
    for (mut sprite, tile) in query.iter_mut() {
        sprite.index = tile.sprite.clone().into();
    }
}
