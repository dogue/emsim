use bevy::{
    prelude::*,
    render::{camera::ScalingMode, texture::ImageSettings},
};

use crate::ui::{UiTile, UiTileFrame};

pub const RESOLUTION: f32 = 16.0 / 9.0;
const CLEAR: Color = Color::BLACK;
const CAMERA_SPEED: f32 = 500.0;
pub const RENDER_EXTENT: f32 = 1000.0;
pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(ClearColor(CLEAR))
            .insert_resource(ImageSettings::default_nearest())
            .insert_resource(WindowDescriptor {
                width: 1600.0,
                height: 900.0,
                title: "EmSim".to_string(),
                ..default()
            })
            .add_startup_system(spawn_camera)
            .add_system(camera_movement);
    }
}

fn spawn_camera(mut commands: Commands) {
    let camera = OrthographicProjection {
        top: RENDER_EXTENT,
        bottom: -RENDER_EXTENT,
        right: RENDER_EXTENT * RESOLUTION,
        left: -RENDER_EXTENT * RESOLUTION,
        scaling_mode: ScalingMode::None,
        ..default()
    };

    commands.spawn_bundle(Camera2dBundle {
        projection: camera,
        ..default()
    });
}

fn camera_movement(
    time: Res<Time>,
    input: Res<Input<KeyCode>>,
    mut query: Query<&mut Transform, With<Camera>>,
    mut selected: Query<&mut Transform, (With<UiTile>, Without<Camera>)>,
    mut frame: Query<&mut Transform, (With<UiTileFrame>, Without<UiTile>, Without<Camera>)>,
) {
    let mut camera = query.single_mut();
    let mut selected = selected.single_mut();
    let mut frame = frame.single_mut();

    if input.pressed(KeyCode::A) {
        camera.translation.x -= CAMERA_SPEED * time.delta_seconds();
        selected.translation.x -= CAMERA_SPEED * time.delta_seconds();
        frame.translation.x -= CAMERA_SPEED * time.delta_seconds();
    }
    if input.pressed(KeyCode::D) {
        camera.translation.x += CAMERA_SPEED * time.delta_seconds();
        selected.translation.x += CAMERA_SPEED * time.delta_seconds();
        frame.translation.x += CAMERA_SPEED * time.delta_seconds();
    }
    if input.pressed(KeyCode::W) {
        camera.translation.y += CAMERA_SPEED * time.delta_seconds();
        selected.translation.y += CAMERA_SPEED * time.delta_seconds();
        frame.translation.y += CAMERA_SPEED * time.delta_seconds();
    }
    if input.pressed(KeyCode::S) {
        camera.translation.y -= CAMERA_SPEED * time.delta_seconds();
        selected.translation.y -= CAMERA_SPEED * time.delta_seconds();
        frame.translation.y -= CAMERA_SPEED * time.delta_seconds();
    }
}
