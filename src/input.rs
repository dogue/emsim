use crate::{
    camera::RESOLUTION,
    grid::GridPosition,
    tile::Tile,
    tile::TILE_SCALE,
    tile::{TileSheet, TileSprite},
    ui::{MousePositionText, MouseTargetText, UiTile},
};
use bevy::prelude::*;

pub struct SelectedTile(TileSprite);

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(update_mouse_text)
            .add_system(mouse_click)
            .add_system(select_tile)
            .insert_resource(SelectedTile(TileSprite::Tree));
    }
}

fn mouse_click(
    windows: Res<Windows>,
    camera: Query<(&Camera, &GlobalTransform)>,
    buttons: Res<Input<MouseButton>>,
    mut commands: Commands,
    mut grid: Query<(&GridPosition, &mut Tile, Entity)>,
    tile_sheet: Res<TileSheet>,
    selected: Res<SelectedTile>,
) {
    let window = windows.get_primary().unwrap();
    let cursor_pos: Vec2;
    let (camera, camera_transform) = camera.single();

    if let None = window.cursor_position() {
        return;
    } else {
        cursor_pos = window.cursor_position().unwrap();
    }

    let world_pos = cursor_to_world(cursor_pos, window, camera, camera_transform);
    let mouse_grid = GridPosition::from_world(world_pos);

    if buttons.just_pressed(MouseButton::Left) {
        for (pos, _tile, _entity) in grid.iter_mut() {
            if pos == &mouse_grid {
                return;
            }
        }

        commands
            .spawn_bundle(SpriteSheetBundle {
                sprite: TextureAtlasSprite {
                    index: 16,
                    ..default()
                },
                texture_atlas: tile_sheet.handle(),
                transform: Transform {
                    translation: Vec3::new(12.0 * TILE_SCALE, 0.0, 1.0),
                    scale: Vec3::splat(TILE_SCALE * RESOLUTION),
                    ..default()
                },
                ..default()
            })
            .insert(Tile::new().with_sprite(selected.0.clone()))
            .insert(mouse_grid);
        return;
    }

    if buttons.just_pressed(MouseButton::Right) {
        for (pos, _tile, entity) in grid.iter() {
            if pos == &mouse_grid {
                commands.entity(entity).despawn_recursive();
            }
        }
    }
}

fn update_mouse_text(
    mut mouse_text: Query<&mut Text, With<MousePositionText>>,
    mut target_text: Query<&mut Text, (With<MouseTargetText>, Without<MousePositionText>)>,
    windows: Res<Windows>,
    camera: Query<(&Camera, &GlobalTransform)>,
) {
    let window = windows.get_primary().unwrap();
    let (camera, camera_transform) = camera.single();
    let cursor_pos: Vec2;

    if let None = window.cursor_position() {
        return;
    } else {
        cursor_pos = window.cursor_position().unwrap();
    }

    for mut text in mouse_text.iter_mut() {
        let world_pos = cursor_to_world(cursor_pos, window, &camera, &camera_transform);
        text.sections[1].value = format!("{}, {}", world_pos.x as i32, world_pos.y as i32);
    }

    for mut text in target_text.iter_mut() {
        let world_pos = cursor_to_world(cursor_pos, window, &camera, &camera_transform);
        let target = GridPosition::from_world(world_pos);

        text.sections[1].value = format!("{}, {}", target.x(), target.y());
    }
}

fn cursor_to_world(
    cursor_pos: Vec2,
    window: &Window,
    camera: &Camera,
    camera_transform: &GlobalTransform,
) -> Vec3 {
    let window_size = Vec2::new(window.width() as f32, window.height() as f32);
    let ndc = (cursor_pos / window_size) * 2.0 - Vec2::ONE;
    let ndc_to_world = camera_transform.compute_matrix() * camera.projection_matrix().inverse();
    ndc_to_world.project_point3(ndc.extend(-1.0))
}

fn select_tile(
    input: Res<Input<KeyCode>>,
    mut tile: Query<&mut Tile, With<UiTile>>,
    mut selected: ResMut<SelectedTile>,
) {
    let mut tile = tile.single_mut();

    if input.just_pressed(KeyCode::Key1) {
        tile.set_sprite(TileSprite::Tree);
        selected.0 = TileSprite::Tree;
    }
    if input.just_pressed(KeyCode::Key2) {
        tile.set_sprite(TileSprite::Campfire);
        selected.0 = TileSprite::Campfire;
    }
    if input.just_pressed(KeyCode::Key3) {
        tile.set_sprite(TileSprite::Dirt);
        selected.0 = TileSprite::Dirt;
    }
}
