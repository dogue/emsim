use crate::camera::RESOLUTION;
use crate::tile::{TILE_SCALE, TILE_SIZE};
use bevy::prelude::*;

pub struct GridPlugin;

impl Plugin for GridPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_to_stage(CoreStage::PostUpdate, enforce_grid);
    }
}

#[derive(Component, PartialEq, Eq)]
pub struct GridPosition {
    x: i32,
    y: i32,
}

#[allow(dead_code)]
impl GridPosition {
    pub fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }

    /// Creates a new [`GridPosition`] from world coordinates
    pub fn from_world(translation: Vec3) -> Self {
        let x = translation.x / (TILE_SIZE * TILE_SCALE * RESOLUTION);
        let y = translation.y / (TILE_SCALE * TILE_SIZE * RESOLUTION);

        Self {
            x: x.round() as i32,
            y: y.round() as i32,
        }
    }

    /// Translates the [`GridPosition`] into world coordinates
    pub fn to_world(&self) -> Vec3 {
        let x = (TILE_SCALE * TILE_SIZE * RESOLUTION / 2.0) * self.x as f32 * 2.0;
        let y = (TILE_SCALE * TILE_SIZE * RESOLUTION / 2.0) * self.y as f32 * 2.0;
        Vec3::new(x, y, 0.0)
    }

    /// Increments the Y axis by `distance` cells
    pub fn up(&mut self, distance: i32) {
        self.y += distance;
    }

    /// Decrements the Y axis by `distance` cells
    pub fn down(&mut self, distance: i32) {
        self.y -= distance;
    }

    /// Decrements the X axis by `distance` cells
    pub fn left(&mut self, distance: i32) {
        self.x -= distance;
    }

    /// Increments the X axis by `distance` cells
    pub fn right(&mut self, distance: i32) {
        self.x += distance;
    }

    pub fn x(&self) -> i32 {
        self.x
    }

    pub fn y(&self) -> i32 {
        self.y
    }
}

impl Default for GridPosition {
    fn default() -> Self {
        Self { x: 0, y: 0 }
    }
}

fn enforce_grid(mut query: Query<(&mut Transform, &GridPosition)>) {
    for (mut transform, pos) in query.iter_mut() {
        transform.translation = pos.to_world();
    }
}
