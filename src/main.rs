use bevy::prelude::*;

mod tile;
use tile::TilePlugin;

mod camera;
use camera::CameraPlugin;

mod input;
use input::InputPlugin;

mod ui;
use ui::UiPlugin;

mod grid;
use grid::GridPlugin;

fn main() {
    App::new()
        .add_plugin(TilePlugin)
        .add_plugin(CameraPlugin)
        .add_plugin(UiPlugin)
        .add_plugin(InputPlugin)
        .add_plugin(GridPlugin)
        .add_plugins(DefaultPlugins)
        .run();
}
