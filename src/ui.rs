use crate::camera::RESOLUTION;
use crate::tile::{Tile, TileSheet, TileSprite, TILE_SCALE};
use bevy::prelude::*;

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(load_font)
            .add_startup_system(load_selected_frame)
            .add_startup_system(selected_tile_setup);
    }
}

#[derive(Component)]
pub struct MousePositionText;

#[derive(Component)]
pub struct MouseTargetText;

#[derive(Component)]
pub struct UiTile;

#[derive(Component)]
pub struct UiTileFrame;

fn selected_tile_setup(mut commands: Commands, texture_atlas: Res<TileSheet>) {
    commands
        .spawn_bundle(SpriteSheetBundle {
            sprite: TextureAtlasSprite {
                index: TileSprite::Tree.into(),
                ..default()
            },
            texture_atlas: texture_atlas.handle(),
            transform: Transform {
                translation: Vec3::new(1680.0, 900.0, 5.0),
                scale: Vec3::splat(RESOLUTION * TILE_SCALE * 1.2),
                ..default()
            },
            ..default()
        })
        .insert(Tile::new().with_sprite(TileSprite::Tree))
        .insert(UiTile);
}

fn load_selected_frame(mut commands: Commands, asset_server: Res<AssetServer>) {
    let frame: Handle<Image> = asset_server.load("selected_frame.png");

    commands
        .spawn_bundle(SpriteBundle {
            texture: frame,
            transform: Transform {
                translation: Vec3::new(1680.0, 900.0, 6.0),
                scale: Vec3::splat(RESOLUTION * TILE_SCALE * 1.2),
                ..default()
            },
            ..default()
        })
        .insert(UiTileFrame);
}

fn load_font(mut commands: Commands, asset_server: Res<AssetServer>) {
    let font = asset_server.load("fonts/FiraMono-Medium.ttf");
    let text_style = TextStyle {
        font,
        font_size: 18.0,
        color: Color::WHITE,
    };

    let window_coords = TextBundle::from_sections([
        TextSection::new("World: ", text_style.clone()),
        TextSection::from_style(text_style.clone()),
    ])
    .with_style(Style {
        position_type: PositionType::Absolute,
        position: UiRect {
            left: Val::Px(5.0),
            top: Val::Px(5.0),
            ..default()
        },
        ..default()
    });

    let target_coords = TextBundle::from_sections([
        TextSection::new("Grid: ", text_style.clone()),
        TextSection::from_style(text_style.clone()),
    ])
    .with_style(Style {
        position_type: PositionType::Absolute,
        position: UiRect {
            top: Val::Px(20.0),
            left: Val::Px(5.0),
            ..default()
        },
        ..default()
    });

    commands
        .spawn_bundle(window_coords)
        .insert(MousePositionText);

    commands.spawn_bundle(target_coords).insert(MouseTargetText);
}
